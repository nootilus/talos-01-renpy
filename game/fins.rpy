# Les Chroniques de Talos - Chapitre 1
## Le trésor de Mallemort

### © 2018-2019 Vincent Corlaix - vcorlaix@nootilus.com

###################### FINS

### Fin n°1 - Le joueur abandonne Latrémon

label dialFIN01:
    scene b_fGen with fx_trans

    pov "Adieu, et bonne chance."
    pause(2)
    pr "Oui, adieu. Bon..."

    scene black with fx_trans

    call attenteTxt(2) from _call_attenteTxt
    
    sys "<Connexion perdue>"
    pause(1)
    sys "Vous n'avez pas voulu aider le professeur à sortir des catacombes."
    sys "C'est de la non-assistance à personne en danger, savez-vous ?"
    sys "D'autant que personne ne l'a revu depuis."
    sys "Vous avez sa disparition sur la conscience..."

    return

### Fin n°2 - Latrémon fuit l’Ombre, la mezzanine s’effondre
label dialFIN02:
    scene b_fGen with fx_trans

    scene black with fx_trans

    call attenteTxt(2) from _call_attenteTxt_1
    
    sys "<Connexion perdue>"
    pause(1)
    sys "Michel Latrémon est mort, écrasé par un éboulement."
    sys "Il aurait pu aller plus loin avec votre aide."
    sys "Voulez-vous recommencer et faire d'autres choix ?"

    return

### Fin n°3 - Latrémon confronte l’Ombre qui lui fait perdre la raison
label dialFIN03:
    scene b_fGen with fx_trans

    scene black with fx_trans

    call attenteTxt(2) from _call_attenteTxt_2
    
    sys "<Connexion perdue>"
    pause(1)
    sys "Michel Latrémon a été retrouvé errant, quelques jours plus tard."
    sys "Il a été interné, le pauvre homme avait complètement perdu l'esprit"
    sys "Il aurait pu aller plus loin avec votre aide."
    sys "Voulez-vous recommencer et faire d'autres choix ?"

    return

### Fin n°4 - Latrémon se noie dans le tunnel
label dialFIN04:
    scene b_fGen with fx_trans

    scene black with fx_trans

    call attenteTxt(2) from _call_attenteTxt_3
    
    sys "<Connexion perdue>"
    pause(1)
    sys "Michel Latrémon est mort noyé."
    sys "Il aurait pu aller plus loin avec votre aide."
    sys "Voulez-vous recommencer et faire d'autres choix ?"

    return

### Fin n°5 - Latrémon passe le portail ---> Chapitre 2
label dialFIN05:
    scene b_fGen with fx_trans

    scene black with fx_trans

    call attenteTxt(2) from _call_attenteTxt_4
    
    sys "<Connexion perdue>"
    pause(1)
    sys "Michel Latrémon a disparu dans une autre dimension."
    sys "Malgré sa disparition, vous l'avez aidé à rester vivant."
    sys "Qui sait, peut-être trouvera-t-il le moyen de rentrer dans notre monde ?"

    return

### Fin n°6 - Latrémon rendu fou en parlant à l’Ombre
label dialFIN06:
    scene b_fGen with fx_trans

    scene black with fx_trans

    call attenteTxt(2) from _call_attenteTxt_5
    
    sys "<Connexion perdue>"
    pause(1)
    sys "Michel Latrémon a été retrouvé errant, quelques jours plus tard."
    sys "Il a été interné, le pauvre homme avait complètement perdu l'esprit"
    sys "Il aurait pu aller plus loin avec votre aide."
    sys "Voulez-vous recommencer et faire d'autres choix ?"

    return

### Fin n°7 - Latrémon rendu fou en parlant à l’Ombre
label dialFIN07:
    scene b_fGen with fx_trans

    scene black with fx_trans

    call attenteTxt(2) from _call_attenteTxt_6
    
    sys "<Connexion perdue>"
    pause(1)
    sys "Le corps de Michel Latrémon a été retrouvé éventré et atrocement mutilé."
    sys "Personne n'a été capable d'expliquer sa présence ici, ni l'état de son cadavre."
    sys "Voulez-vous recommencer et faire d'autres choix ?"

    return