# Les Chroniques de Talos - Chapitre 1
## Le trésor de Mallemort

### © 2018-2019 Vincent Corlaix - vcorlaix@nootilus.com

###################### À RETIRER AVANT PROD !!!
define debugJump = 601
define okDebug = False

###################### INTRO

### Écran d'intro
label splashscreen:
    scene black
    with Pause(1)

    show text "Les Chroniques de Talos\n\nChapitre 1\nLe trésor de Malemont" with fx_trans
    with Pause(2)

    hide text with fx_trans
    with Pause(1)

    return

###################### DÉFINITIONS ET IMAGES

### Définition des intervenants
define pov = Character("[joueur]", color="#1ae81a", image="p_joueur")
define pr = Character('Pr. Latrémon', color="#ffb000", image="p_prof")
define sys = Character('Système', color="#ff0000", image="p_systeme")
define ecrit = Character('Réception d\'un message...', color="#ffc000", image="p_wait", what_font="lucon.ttf")
     
### Image animée «en train d'écrire»
image side p_joueur:
    "p_pov.png"
    xalign 1.0 yalign 1.0
image side p_prof = "p_prof.png"
image side p_systeme = "p_sys.png"
image side p_wait:
    "p_wait0.png"
    pause 0.5
    "p_wait1.png"
    pause 0.2
    "p_wait2.png"
    pause 0.2
    "p_wait3.png"
    pause 0.2
    repeat

### Images de fond du jeu
image b_fGen = "bg_debut.jpg"

image b_choix = "ecran_choix.png"
image b_intro = "bg_debut.jpg"
image b_tourne = "bg_tourneRond.jpg"
image b_peinture = "bg_peinture.jpg"
image b_tunnel = "bg_tunnel.jpg"
image b_escalier = "bg_escalier.jpg"
image b_crypte = "bg_crypte.jpg"

###################### EFFETS

### Effets de transitions
define fx_trans = Dissolve(0.2)

transform change_transform(old, new):
    contains:
        old
        yalign 1.0
        xpos 0.0 xanchor 0.0
        linear 0.2 xanchor 1.0
    contains:
        new
        yalign 1.0
        xpos 0.0 xanchor 1.0
        linear 0.2 xanchor 0.0

define config.side_image_change_transform = change_transform

###################### FONCTIONS

### Fonction 1 - Pause «en train d'écrire» version 2
screen pasClic(temps):
    timer temps action Hide("pasClic")
    key "mouseup_1" action NullAction()

### Fonction 2 - Une pause enter-dialogue
label attenteTxt(temps):
    pause(temps)
    show screen pasClic(2)
    ecrit " {w=2.0}{size=-5}{color=#ee5511}Message reçu. Cliquez pour le lire...{/color}{/size}"
    return

    
###################### VARIABLES

### Booléenne d'embranchements des réponses du joueur
define choixDial = False
