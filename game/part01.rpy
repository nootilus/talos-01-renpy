# Les Chroniques de Talos - Chapitre 1
## Le trésor de Mallemort

### © 2018-2019 Vincent Corlaix - vcorlaix@nootilus.com

###################### PARTIE 1

label start:

    $ randCon = renpy.random.randint(10000, 60000)

    python:
        joueur = renpy.input("Entrez votre prénom ou pseudonyme pour accéder au chat >", allow="abcdefghijklmnopqrstuvwxyzàÀéÉèÈîÎïÏôÔABCDEFGHIJKLMNOPQRSTUVWXYZ-", length=40).capitalize().strip()

        if not joueur:
            joueur = "User" + str(randCon)

    sys "Bienvenue sur le réeau AE-Com."

    if okDebug == True:
        $ renpy.jump("dial" + str(debugJump))

    show screen pasClic(2)
    ecrit ' {w=2.0}{size=-5}{color=#ee5511}Message reçu. Cliquez pour le lire...{/color}{/size}'

    show b_intro with fx_trans

    pr "Ah, bon sang, comment marche ce truc ?..."
    pr "Allô ?"

    pause(2)

    pr "Oh..."
    pr "C'est génial ! Je vois ce que je dis sur l'écran !"
    pr "Je n'ai pas besoin de taper."
    pr "Bon."

    call attenteTxt(2) from _call_attenteTxt_7

    pr "Mais, je ne sais même pas si le réseau passe ici..."

    call attenteTxt(1) from _call_attenteTxt_8

    pr "Y'a quelqu'un ?"

    scene b_choix with fx_trans

menu:

    "Oui.":
        jump dial102

    "Qui êtes-vous ?":
        jump dial103

label dial102:
    scene b_intro with fx_trans

    pov "Oui. Je vous reçois."
    call attenteTxt(2) from _call_attenteTxt_9


    pr "Oh, quelle chance !"
    pr "J'étais presque sûr de ne pas accrocher de réseaux, ici."
    pr "À moins que..."
    pause(1)
    pr "Vous n'êtes pas dans le souterrain avec moi, quand même ?"

    $choixDial = False

    scene b_choix with fx_trans

menu:
    "Non, je suis chez moi.":
        jump dial104

    "Qui êtes-vous ?":
        $ choixDial = True
        jump dial103

label dial103:
    scene b_intro with fx_trans

    if choixDial == False:
        pov "Je reçois vos messages, mais qui êtes-vous au juste ?"
    else:
        pov "Mais enfin, qui êtes-vous au juste ? C'est quoi, cette histoire de souterrains ?"
    call attenteTxt(2) from _call_attenteTxt_10

    pr "Oui, j'aurai dû commencer par là, désolé."
    pr "C'est à dire que je ne suis pas dans une situation qui pousse aux civilités."

    jump dial200

label dial104:
    scene b_intro with fx_trans

    pov "Non, je suis chez moi. De quels souterrains vous parlez ?"
    call attenteTxt(2) from _call_attenteTxt_11

    pr "C'est un peu long à raconter, mais pour résumer..."
    pr "Je suis dans des catacombes, et je me suis perdu."
    pause(1)
    pr "Mais je manque à toutes les politesses."

    call attenteTxt(2) from _call_attenteTxt_12

    jump dial200

label dial200:
    scene b_intro with fx_trans

    pr "Je m'appelle Michel Latrémon, et je suis historien."
    pr "Je sais que, quand je dis ma profession, on pense à un homme prématurément vieilli, le nez dans des bouquins aussi moisis que lui."
    pr "Et, croyez-moi, c'est habituellement le cas."
    pr "Mais, je suis tombé il y a quelques mois sur un manuscrit intéressant."
    pause(1)
    pr "Connaissez-vous Jean de Malemont ?"

    scene b_choix with fx_trans

menu:
    "Pas du tout.":
        jump dial201

    "Quel nom affreux !":
        jump dial202

    "Ce n'était pas un templier ?":
        jump dial203

label dial201:
    scene b_intro with fx_trans

    pov "Non. Ça ne me dit rien du tout."
    call attenteTxt(2) from _call_attenteTxt_13

    pr "Jean de Malemont était sergent du Temple à Limoges, de 1304 à 1310."
    pr "Lorsque l'ordre des Templiers a été détruit, il a été fait prisonnier."
    pr "Mais il a échappé mystérieusement au bûcher."
    pause(1)
    pr "J'ai retrouvé sa trace par accident."
    pr "Et je n'ai pas pu résister à l'envie de voir par moi-même si son tombeau est bien là où le manuscrit le mentionnait."

    $choixDial = True

    scene b_choix with fx_trans

menu:
    "Qu'est-ce qui vous arrive ?":
        jump dial300

    "Pourquoi me contactez-vous ?":
        jump dial301

label dial202:
    scene b_intro with fx_trans

    pov "Quel nom affreux !"
    call attenteTxt(2) from _call_attenteTxt_14

    pr "Hahaha, oui."
    pr "Mais ce n'est qu'un nom. Ce n'était pas un «méchant»."
    pause(1)
    pr "Enfin, je ne crois pas..."

    $choixDial = False
    jump dial300

label dial203:
    scene b_intro with fx_trans

    pov "Le nom me dit quelque chose. Ce n'était pas un templier ?"
    call attenteTxt(2) from _call_attenteTxt_15

    pr "Si, exactement !"
    pr "Whow, vous ne seriez pas historien, vous aussi ?"
    pause(1)
    pr "Hum..."
    pr "Ou alors... Vous avez regardé sur Google, n'est-ce pas ?"
    pause(1)
    pr "Ha ha ha !"
    pr "Je ne vous en veux pas."

    $choixDial = False
    jump dial300

label dial300:
    scene b_intro with fx_trans

    if choixDial == True:
        pov "Que vous est-il arrivé ? On dirait que vous avez besoin d'aide."
        call attenteTxt(2) from _call_attenteTxt_16

    pr "Il m'est arrivé une guigne pas possible."
    pr "J'avoue, j'ai commencé mon exploration en complet dilettante."
    pr "Je ne suis ni archéologue, ni spéléologue."
    pause(1)
    pr "Je me suis simplement dit que, si c'était trop dangereux ou compliqué, je ferais demi-tour et reviendrais avec une équipe d'étudiants."
    pr "Après tout, il faut bien qu'ils aient leur utilité."
    pause(1)
    pr "Enfin bref…"

    $choixDial = False
    jump dial301

label dial301:
    scene b_intro with fx_trans

    if choixDial == True:
        pov "Pourquoi vous me contactez ? Qu'attendez-vous de moi ?"
        call attenteTxt(2) from _call_attenteTxt_17

    pr "Si je vous ai contacté, c'est par accident."
    pr "Un heureux accident pour moi. Peut-être ennuyeux pour vous, par contre."
    pr "Néanmoins, je voulais appeler des secours sur mon portable."
    pr "Et... Bon... Comment dire ?"
    pause(1)
    pr "Je n'aime pas ces appareils, je les trouve trop compliqués."
    pr "Donc, au lieu de composer le bon numéro, j'ai sans doute appuyé où il ne fallait pas."
    pr "Et j'ai lancé cette application de discussion en réseau."
    pause(1)
    pr "Mais si vous pouviez m'aider, ce serait tellement aimable de votre part..."

    scene b_choix with fx_trans

menu:
    "Volontiers.":
        $choixDial = False
        jump dial303

    "Je n'ai pas le temps.":
        jump dial302

label dial302:
    scene b_intro with fx_trans

    pov "Désolé, je n'ai pas le temps. Essayez plutôt d'appeler les secours."
    call attenteTxt(2) from _call_attenteTxt_18

    pr "Ah."
    pr "Très bien."
    pause(2)
    pr "Désolé de vous avoir dérangé."
    pr "Je vais essayer de me débrouiller par moi-même."
    pause(1)
    pr "Adieu."

    scene b_choix with fx_trans

menu:
    "Hum... Attendez.":
        $choixDial = True
        jump dial303

    "Adieu.":
        jump dialFIN01

label dial303:
    scene b_intro with fx_trans

    if choixDial == False:
        pov "Je veux bien vous aider, même si je ne suis pas sûr de pouvoir faire grand-chose."
    else:
        pov "Je ne sais pas trop ce que je peux faire, mais dites toujours ce que vous attendez de moi."
    call attenteTxt(2) from _call_attenteTxt_19

    pr "Oh, merveilleux ! Merci, merci !"
    pr "Vous n'imaginez pas à quel point je suis soulagé."
    pause(1)
    pr "Bon, je vous explique ma situation."
    pr "J'ai trouvé les catacombes, comme décrites dans le manuscrit."
    pr "J'ai commencé à les explorer, mais c'est un vrai dédale."
    pr "J'ai noté mes déplacements dans un petit carnet."
    pr "Mais je suis passé par une section où l'eau s'est infiltrée."
    pr "Le sol était couvert de plusieurs centimètres de boue."
    pr "Avec ma petite lampe, je n'ai rien vu."
    pause(1)
    pr "Bref. J'ai glissé."
    pr "Et mon carnet est tombé. Complètement trempé, l'encre délavée..."
    pause(1)
    pr "Pour vous le dire clairement : je n'ai plus de carte."
    pr "Et je ne trouve plus mes repères."
    pause(1)
    pr "Je suis perdu."

    scene b_choix with fx_trans

menu:
    "Quels choix avez-vous ?":
        jump dial400

label dial400:
    scene b_intro with fx_trans

    pov "Quelles directions s'offrent à vous, maintenant ?"
    call attenteTxt(2) from _call_attenteTxt_20

    pr "Je n'en ai guère."
    pr "Je suis dans un couloir qui forme un coude devant et derrière moi."
    pr "Il y a un effondrement du sol dans lequel je peux me glisser."
    pr "Mais j'ignore s'il s'agit d'un passage."
    pause(1)
    pr "Vous feriez quoi, à ma place ?"

    scene b_choix with fx_trans

menu:
    "Suivez le couloir.":
        jump dial401

    "Descendez par le puits.":
        $choixDial = False
        jump dial500

label dial401:
    scene b_intro with fx_trans
    pov "Essayez de suivre le couloir. Ça semble le plus logique."
    call attenteTxt(2) from _call_attenteTxt_21

    pr "Ok. Merci."
    pr "Allons-y, gaiement !"
    pr "Je vous ai dit que j'avais été scout ?"
    pr "Mais je ne vais pas me mettre à chanter, rassurez-vous."
    pr "Bon, je vais épargner mon souffle."
    pr "Je vous recontacte quand je trouve quelque chose."

    call attenteTxt(5) from _call_attenteTxt_22

    scene b_tourne with fx_trans
    pr "Heu, vous êtes là ?"

    scene b_choix with fx_trans

menu:
    "Oui.":
        $choixDial = False
        jump dial403

    "Que se passe-t-il ?":
        $choixDial = True
        jump dial403

label dial403:
    scene b_tourne with fx_trans

    if choixDial == False:
        pov "Oui, Michel. Je suis là."
    else:
        pov "Qu'y-a-t'il ? Vous avez trouvé quelque chose ?"
    call attenteTxt(2) from _call_attenteTxt_23

    pr "Vous n'allez sans doute pas me croire..."
    pause(1)
    pr "Je suis revenu à mon point de départ."
    pr "C'est-à-dire ; je suis à nouveau à l'endroit du trou."
    pr "Là où je vous ai demandé par où aller."
    pr "Et que vous m'avez dit de prendre le couloir."
    pr "J'ai peut-être loupé quelque chose ?"
    pause(1)
    pr "J'ai du mal à croire que j'ai pu tourner en rond."

    scene b_choix with fx_trans

menu:
    "Recommencez.":
        jump dial404

    "Laissez tomber. Essayez de descendre dans le puits.":
        $choixDial = True
        jump dial500

label dial404:
    scene b_tourne with fx_trans

    pov "Vous avez dû louper un passage. Retournez-y."
    call attenteTxt(2) from _call_attenteTxt_24

    pr "Bien. Vous avez probablement raison."
    pr "Et puis qui ne tente rien n'a rien."
    pr "Je suis désolé. Je vais arrêter de vous assommer avec mes aphorismes de vieil universitaire."
    pause(1)
    pr "Je vous tiens au courant de mon exploration."

    call attenteTxt(3) from _call_attenteTxt_25

    pr "Oh, non..."

    scene b_choix with fx_trans

menu:
    "Quoi ?":
        jump dial406

    "Retour au départ ?":
        jump dial407

label dial406:
    scene b_tourne with fx_trans

    pov "Vous avez trouvé quelque chose ?"
    call attenteTxt(2) from _call_attenteTxt_26

    pr "Bingo."
    pr "Je n'y crois pas."

    call attenteTxt(4) from _call_attenteTxt_27

    pr "Je suis à nouveau au même endroit."
    pr "Et, je ne vous cache pas que ça me donne la chair de poule."
    pr "Comment vous dire ?"
    pause(1)
    pr "Je ne suis pas quelqu'un d'impressionnable."
    pr "Mais ici, j'ai une sensation étrange."
    pr "Comme le sentiment d'être épié."
    pr "Bon."
    pause(1)
    pr "Je n'ai plus le choix, n'est-ce pas ?"

    scene b_choix with fx_trans

menu:
    "Le trou.":
        jump dial500

    "Le puits.":
        jump dial500

    "L'orifice.":
        jump dial500

    "Le méat.":
        jump dial500

label dial407:
    scene b_tourne with fx_trans

    pov "Ne me dites rien. Vous êtes encore revenu au début ?"
    call attenteTxt(2) from _call_attenteTxt_28

    pr "Je suis à nouveau au même endroit."
    pr "Et, je ne vous cache pas que ça me donne la chair de poule."
    pause(1)
    pr "Comment vous dire ?"
    pr "Je ne suis pas quelqu'un d'impressionnable."
    pr "Mais ici, j'ai une sensation étrange."
    pr "Comme le sentiment d'être épié."
    pr "Bon."
    pause(1)
    pr "Je n'ai plus le choix, n'est-ce pas ?"

    scene b_choix with fx_trans

menu:
    "Le trou.":
        jump dial500

    "Le puits.":
        jump dial500

    "L'orifice.":
        jump dial500

    "Le méat.":
        jump dial500

label dial500:
    scene b_tourne with fx_trans

    if choixDial == False:
        pov "Peut-être que ce trou mène plus facilement dehors."
    else:
        pov "Laissez tomber. Essayez de descendre dans le puits."
    call attenteTxt(2) from _call_attenteTxt_29

    pr "Bien."
    pr "Un peu de courage."
    pr "Je suis désolé, mais comme je vais devoir m'aider de mes mains, je vais mettre mon téléphone dans une poche."
    pr "Je vous retrouve en bas."
    pause(1)
    pr "Enfin, j'espère."
    pr "À tout de suite !"

    call attenteTxt(5) from _call_attenteTxt_30
    scene b_peinture with fx_trans

    pr "Oh, bon sang !"

    scene b_choix with fx_trans

menu:
    "Michel ? ça va ?":
        jump dial502

    "Que se passe-t-il ?":
        jump dial503

label dial502:
    scene b_peinture with fx_trans
    pov "Michel, vous allez bien ? Vous ne vous êtes pas blessé ?"
    call attenteTxt(2) from _call_attenteTxt_31

    pr "Non, non. Ne vous inquiétez pas."
    pr "Tout va bien."
    pr "C'est juste que..."
    pause(1)
    pr "C'est fou !"
    pr "Vous n'allez pas me croire."

    $choixDial = True
    jump dial503

label dial503:
    scene b_peinture with fx_trans
    if choixDial == False:
        pov "Qu'est-ce qui se passe ? Vous avez trouvé quelque chose ?"
    call attenteTxt(2) from _call_attenteTxt_32

    pr "Je suis dans une salle. Mais ça n'a rien à voir avec les catacombes au-dessus."
    pr "Ce lieu est plus ancien que le réseau de tunnels au-dessus de moi."
    pr "Je ne peux, bien entendu, rien affirmer avec certitude."
    pr "Mais il y a des détails architecturaux qui ne trompent pas."
    pr "Et puis, à l'époque, on empilait."
    pr "On construisait rarement en dessous de l'existant."
    pause(1)
    pr "Pourtant, il y a là quelque chose digne de troubler le vieil historien que je suis."

    scene b_choix with fx_trans

menu:
    "Qu'avez-vous trouvé ?":
        $choixDial = True
        jump dial505

    "Vous êtes sûr d'aller bien ?":
        jump dial504

label dial504:
    scene b_peinture with fx_trans
    pov "Vous êtes sûr de ne pas vous être cogné la tête en descendant ?"
    call attenteTxt(2) from _call_attenteTxt_33

    pr "Hum. Vous avez le même humour que mes étudiants."
    pr "Mais pardonnez mon émotion."
    pause(1)
    pr "Je suis devant quelque chose d'incompréhensible, historiquement parlant."

    jump dial505

label dial505:
    scene b_peinture with fx_trans
    if choixDial == True:
        pov "Qu'est-ce qui vous étonne tant dans votre découverte, Michel ?"
    call attenteTxt(2) from _call_attenteTxt_34

    pr "Comme je vous l'ai dit, la partie souterraine où je me trouve maintenant est forcément plus vieille que la précédente."
    pr "Le niveau que je viens de quitter date de l'époque du templier de Malemont."
    pr "Or, j'ai là sous les yeux une fresque peinte à même le mur."
    pr "Et elle représente sans aucun doute Jean de Malemont, dans son armure de templier."
    pr "Il y est représenté combattant des démons et d'autres créatures impies qui sortent d'un disque de lumière bleue."
    pr "Malemont tient son épée d'une main et brandit un livre de l'autre."
    pr "Le livre rayonne, et il est représenté comme s'il était plus puissant que l'épée du templier."
    pr "Sur chaque tableau, Jean semble suivi par une grande ombre, un nuage noir."
    pause(1)
    pr "Il y a un texte en latin, en dessous."
    pr "Laissez-moi quelques minutes, que je le traduise."

    call attenteTxt(6) from _call_attenteTxt_35

    pr "Hon hon..."
    pr "Très intéressant."

    scene b_choix with fx_trans

menu:
    "Qu'est-ce que ça dit ?":
        jump dial508

    "Arrêtez ce suspence !":
        jump dial507

label dial507:
    scene b_peinture with fx_trans
    pov "Ne jouez pas avec mes nerfs, Michel. Dites-moi ce que vous avez lu !"
    call attenteTxt(2) from _call_attenteTxt_36

    pr "Pardonnez-moi. C'est si passionnant !"
    pr "Le naturel revient vite au galop devant un tel morceau d'histoire."
    pr "Surtout une histoire oubliée et si étrange."

    $choixDial = True
    jump dial508

label dial508:
    scene b_peinture with fx_trans
    if choixDial == True:
        pov "Alors, vous avez pu tout lire ? Qu'est-ce que ça raconte ?"
    else:
        pov "Ne jouez pas avec mes nerfs, Michel. Dites-moi ce que vous avez lu !"
    call attenteTxt(2) from _call_attenteTxt_37

    pr "Il est décrit comment Jean de Malemont a su échapper à ses ennemis et les terrasser grâce à un livre."
    pr "Ce livre est celui représenté sur la fresque."
    pr "Il porte un nom. Malheureusement, même si ça ressemble à du latin, cela ne veut rien dire, pour moi."
    pr "Le «{i}Libris Daemonaticaspius{/i}»."

    scene b_choix with fx_trans

menu:
    "Il y a «{i}daemon{/i}» dedans.":
        jump dial509
    "Et ensuite ?":
        $choixDial = True
        jump dial510

label dial509:
    scene b_peinture with fx_trans
    pov "Il y a le mot «{i}daemon{/i}». Ça ressemble beaucoup à «{i}démon{/i}», non ?"
    call attenteTxt(2) from _call_attenteTxt_38

    pr "Héhéhé, oui."
    pr "Mais ne vous en formalisez pas trop. Les démons étaient courants à l'époque."
    pr "Surtout dans la littérature religieuse et l'imaginaire collectif."
    pr "Tout ce que l'esprit humain de l'époque ne comprenait pas était forcément d'origine divine ou démoniaque."
    pr "Enfin, bref...    "

    jump dial510

label dial510:
    scene b_peinture with fx_trans
    if choixDial == True:
        pov "Qu'est-ce que le texte dit d'autre ?"
    call attenteTxt(2) from _call_attenteTxt_39

    pr "Le texte explique qu'entre autres pouvoirs, ce livre permet à son possesseur de se rendre dans... «{i}d'autres endroits{/i}»."
    pr "Je n'arrive pas à traduire ce passage plus clairement."
    pr "Et... Oh !"

    scene b_choix with fx_trans

menu:
    "Michel ?":
        $choixDial = False
        jump dial511
    "Quoi encore ?":
        $choixDial = True
        jump dial511

label dial511:
    scene b_peinture with fx_trans
    if choixDial == False:
        pov "Michel, qu'y a-t-il ?"
    else:
        pov "Qu'avez-vous déniché encore ?"
    call attenteTxt(2) from _call_attenteTxt_40

    pr "C'est incroyable !"
    pr "D'après ce texte, le «{i}Libris Daemonaticaspius{/i}» serait ici même, dans cette crypte."
    pr "C'est inespéré !"

    scene b_choix with fx_trans

menu:
    "C'est un trésor historique !":
        jump dial512
    "Vous comptez l'utiliser ?":
        jump dial513

label dial512:
    scene b_peinture with fx_trans
    pov "Vous devez le retrouver ! C'est une relique historique inestimable."
    call attenteTxt(2) from _call_attenteTxt_41

    pr "Hahaha ! Vous voilà piqué par le virus de l'historien."
    pr "Oui, certes, c'est un trésor incroyable."
    pr "Mais plus encore."
    pause(1)
    pr "Si ses pouvoirs sont réels, je suis sauvé !"

    jump dial600

label dial513:
    scene b_peinture with fx_trans
    pov "Vous pensez qu'il va vous aider à sortir d'ici par magie, n’est-ce pas ?"
    pov "C'est bien ça que vous avez en tête ? Vous êtes fou !"
    call attenteTxt(2) from _call_attenteTxt_42

    pr "Qui ne tente rien n'a rien, mon jeune ami."
    pr "Et puis, j'ai besoin de me raccrocher à quelque chose."
    pr "Je ne suis pas claustrophobe, mais j'aimerai sortir."
    pr "J'ai l'impression croissante de ne pas être seul, ici."
    pr "Et puis..."
    pause(2)
    pr "Si je reviens à l'université avec ce précieux manuscrit, cela m'évitera sans doute les quolibets de mes collègues."
    pr "Quand ils apprendront comment je me suis retrouvé bêtement enfermé ici, les moqueries ne manqueront pas de pleuvoir."

    jump dial600

label dial600:
    call attenteTxt(2) from _call_attenteTxt_43

    pr "Bien. Peu importe que le livre soit magique ou pas."
    pr "Je n'ai pas d'autre choix que de tenter de trouver cette crypte."
    pr "Il serait fort dommage pour moi que je n'y déniche pas un passage menant à l'extérieur."

    scene b_choix with fx_trans

menu:
    "C'est certain.":
        jump dial601
    "Peut-être.":
        jump dial602

label dial601:
    scene b_peinture with fx_trans
    pov "Vous avez raison. Il faut aller explorer cet endroit, il doit y avoir une issue."
    call attenteTxt(2) from _call_attenteTxt_44

    pr "Votre enthousiasme me réchauffe le cœur."
    $ renpy.say(pr, "Merci de me soutenir, " + str(joueur) + ". Je me sens moins seul, ici.")
    pr "Allez. Hardi, compagnon !"

    jump dial603

label dial602:
    scene b_peinture with fx_trans
    pov "C'est possible, mais risqué. Restez prudent."
    call attenteTxt(2) from _call_attenteTxt_45

    pr "Ne vous inquiétez pas."
    pr "J'ai beau me trouver dans cette situation étrange, je reste quelqu'un de très prudent."
    pause(1)
    pr "Par exemple, je vérifie toujours que mes lacets sont noués avant de sortir."
    pr "Ou encore, je ne prends jamais de frites à la cantine, par peur du cardiologue."
    pause(1)
    pr "Ah. Je digresse encore."
    pr "Mais je tiens à vous remercier pour votre sollicitude."
    pr "Merci de m'accompagner, ça m'aide vraiment beaucoup."

    jump dial603

label dial603:
    call attenteTxt(2) from _call_attenteTxt_46

    pr "Bien. Il est temps de se décider."
    pause(1)
    pr "À gauche, un couloir bas conduit à une volée de marches."
    pr "Derrière moi, en face de la frise, il y a une sorte de tunnel."
    pr "Les deux passages semblent aller dans la même direction."
    pr "J'en déduis que les deux mènent à la crypte."
    pause(2)
    pr "L'escalier bas de plafonds semble être l'accès le plus simple et sûr."
    pr "Mais un truc me chiffonne ; c'est une crypte que je cherche."
    pr "En général, on descend dans une crypte, et on remonte pour en sortir."
    pause(2)
    pr "D'un autre côté, le tunnel est étroit et humide."
    pr "Même si je ne suis pas claustrophobe, j'hésite à m'y glisser."

    call attenteTxt(2) from _call_attenteTxt_47

    pr "Qu'est-ce que vous me conseillez ?"

    scene b_choix with fx_trans

menu:
    "Par l'escalier.":
        jump dial700
    "Par le tunnel.":
        jump dial800

### SÉQUENCE ESCALIER
label dial700:
    scene b_peinture with fx_trans
    if choixDial == False:
        pov "Je suggère l'escalier. C'est le choix le plus prudent."
        call attenteTxt(2) from _call_attenteTxt_48

        pr "Alors soit, grimpons cet escalier !"
        pr "Je vous recontacte quand je suis arrivé... quelque part."

    call attenteTxt(5) from _call_attenteTxt_49
    scene b_escalier with fx_trans

    pr "Oh, bon sang."
    pr "Ce n'est plus de mon âge, tout ça."
    pr "Mais me voilà arrivé."
    pause(1)
    pr "Ça n'était pas très haut, en fait. Mais..."
    pr "Les architectes de l'époque semblaient trouver amusant de ne jamais faire deux marches de la même hauteur."
    pause(1)
    pr "Bref, me voilà dans la crypte."
    scene b_crypte with fx_trans
    pr "Enfin. Au-dessus, plutôt."

    scene b_choix with fx_trans

menu:
    "Que voyez-vous ?":
        jump dial703
    "Et le livre ?":
        jump dial702

label dial702:
    scene b_crypte with fx_trans
    pov "Vous voyez le livre ? Vous pouvez l'atteindre ?"
    call attenteTxt(2) from _call_attenteTxt_50

    pr "Oui, je le vois. Il est là."
    $choixDial = True
    jump dial703

label dial703:
    scene b_crypte with fx_trans
    if choixDial == True:
        pov "Qu'est-ce que vous voyez ?"
    call attenteTxt(2) from _call_attenteTxt_51

    pr "Je surplombe la crypte depuis une mezzanine."
    pr "Le mausolée lui-même semble immense, mais j'ai du mal à juger."
    pr "Le faisceau de ma lampe se perd dans les profondeurs."
    pr "Je vois le sol, en dessous, à peut-être..."
    pr "Oui, une dizaine de mètres."
    pause(2)
    pr "La crypte est vide, il y a des colonnes à intervalle régulier."
    pr "Et, au centre, un piédestal sur lequel se trouve le {i}Libris Daemonaticaspius{/i}."
    pr "Je ne peux pas le louper, ses dorures reflètent la lumière de ma torche."
    pr "Il est magnifique !"

    scene b_choix with fx_trans

menu:
    "Qu'allez-vous faire ?":
        $choixDial = False
        jump dial704
    "Pouvez-vous l’atteindre ?":
        $choixDial = True
        jump dial704

label dial704:
    scene b_crypte with fx_trans
    if choixDial == False:
        pov "Qu'allez-vous faire ?"
    else:
        pov "Vous pouvez l'atteindre ?"
    call attenteTxt(2) from _call_attenteTxt_52

    pr "Hé bien, je..."
    pr "Oh..."
    pause(1)
    pr "Qu'est-ce que..."
    pause(2)
    pr "Ah !..."

    scene b_choix with fx_trans

menu:
    "Michel ? Ça va ?":
        jump dial705

label dial705:
    scene b_crypte with fx_trans
    pov "Michel ? Ça va ? Vous allez bien ? Répondez-moi !"
    call attenteTxt(2) from _call_attenteTxt_53

    pr "Je..."
    pause(1)
    pr "Je n'ose plus bouger."
    pause(2)
    pr "Il y a ... quelque chose devant moi."
    pr "Sur la mezzanine, à quelques mètres."

    scene b_choix with fx_trans

menu:
    "Michel, parlez-moi !":
        $choixDial = False
        jump dial706
    "Qu'est-ce que c'est ?":
        $choixDial = True
        jump dial706

label dial706:
    scene b_crypte with fx_trans
    if choixDial == False:
        pov "Michel, parlez-moi !"
    else:
        pov "Qu'est-ce que c'est ? Que voyez-vous ?"
    call attenteTxt(2) from _call_attenteTxt_54

    pr "C'est... Oh, bon sang !"
    pr "C'est l'Ombre !"
    pr "Celle de la frise. Celle qui suit Jean de Malemont !"
    pause(1)
    pr "Alors, tout est vrai !"

    scene b_choix with fx_trans

menu:
    "Ne restez pas là !":
        jump dial707
    "Descendez de là !":
        jump dial708
    "Ne bougez pas !":
        jump dial709

label dial707:
    scene b_crypte with fx_trans
    pov "Fuyez ! Ne restez pas là ! Foncez !"
    call attenteTxt(2) from _call_attenteTxt_55

    pr "Ah ! Oui !"
    pr "Je fonce !"

    call attenteTxt(3) from _call_attenteTxt_56

    pr "Oh !"
    pr "Le sol tremble !"
    pr "Bon sang !"
    pause(1)
    pr "La mezzanine !"
    pr "Elle s'effondre, je..."
    pr "... haaaa !..."

    pause(2)
    jump dialFIN02

label dial708:
    scene b_crypte with fx_trans
    pov "Sautez, c'est la seule issue !"
    call attenteTxt(2) from _call_attenteTxt_57

    pr "Ok !"
    pr "J'enjambe et je saute !"
    pause(1)
    pr "Aaaah !...   "

    call attenteTxt(4) from _call_attenteTxt_58

    pr "Oh, bon sang !"
    pause(1)
    pr "J'ai dû me fouler la cheville."
    pr "Ça fait un mal de chien !"
    pr "Ouch."

    call attenteTxt(2) from _call_attenteTxt_59

    pr "Bon."
    pause(1)
    pr "Je peux marcher."
    pr "C'est douloureux, mais ça ira."
    pr "Et la bonne nouvelle..."
    pause(1)
    pr "C'est que notre amie l'Ombre ne m'a pas suivie."
    pr "L'autre bonne nouvelle, c'est que je suis dans la crypte."
    pr "Près du livre."

    jump dial900

label dial709:
    scene b_crypte with fx_trans
    pov "Ne bougez surtout pas ! Ne faites pas un geste, pas un bruit."
    call attenteTxt(2) from _call_attenteTxt_60

    pr "Ok, je..."
    pr "Je ne bouge pas."

    call attenteTxt(3) from _call_attenteTxt_61

    pr "Ah, elle..."

    call attenteTxt(2) from _call_attenteTxt_62

    pr "Non, elle se déplace..."
    pr "Elle fonce sur moi !"
    pr "Ahhh, je..."
    pr "Ahhhhh !"

    call attenteTxt(3) from _call_attenteTxt_63

    pr "Ahhh ha ha ha !"
    pr "Héhéhéhéhé... Hiiiiiiiiiii !"

    jump dialFIN03

### SÉQUENCE TUNNEL
label dial800:
    scene b_peinture with fx_trans
    pov "Je prendrais le tunnel. Je ne pense pas que l'autre passage mène à la crypte."
    call attenteTxt(2) from _call_attenteTxt_64

    pr "Ok, je vais donc jouer les taupes."
    pr "Brrrr..."
    pr "C'est vraiment froid. Et humide."
    pr "Si j'attrape des rhumatismes, je leur donnerais votre nom."
    pr "Allez, un peu de courage."

    call attenteTxt(2) from _call_attenteTxt_65
    scene b_tunnel with fx_trans

    pr "Quand je parlais d'humidité..."
    pr "J'ai les pieds dans l'eau."
    pr "Je suis obligé d'avancer courbé en avant tellement c'est bas."
    pause(3)
    pr "Et maintenant mes chaussures sont trempées."
    pr "Ce ne sont pas des chaussures italiennes irremplaçables."
    pr "Je n'ai qu'une paye d'universitaire."
    pr "Mais je n'ai que ça aux pieds, et c'est franchement désagréable."
    pause(2)
    pr "Je me demande si c'était une si bonne idée."

    call attenteTxt(2) from _call_attenteTxt_66

    pr "Vous me croyez si vous voulez, mais j'ai de l'eau jusqu'à la taille."
    pr "Et je ne vois toujours pas la sortie."

    scene b_choix with fx_trans

menu:
    "Faites demi-tour.":
        jump dial803
    "Insistez.":
        jump dial804

label dial803:
    scene b_tunnel with fx_trans
    pov "Rebroussez chemin. C'est plus prudent."
    call attenteTxt(2) from _call_attenteTxt_67

    pr "Je crois que vous avez raison."
    pr "Je remonte vers la salle de la frise."
    pr "Et j'essayerai l'autre passage."

label dial813:
    scene b_tunnel with fx_trans
    call attenteTxt(4) from _call_attenteTxt_68

    pr "Voilà. Je suis remonté."
    pr "J'ai les pieds et les fesses trempés et glacés, mais je respire encore."
    pr "Il ne me reste plus qu'à grimper ces marches pour me réchauffer."

    $choixDial = True

label dial804:
    scene b_tunnel with fx_trans
    pov "Essayez de continuer tant que vous pouvez."
    pov "Ça serait bête de renoncer maintenant."
    call attenteTxt(2) from _call_attenteTxt_69

    pr "D'accord. Je continue."

    call attenteTxt(5) from _call_attenteTxt_70

    pr "Oh, là là !"
    pr "J'ai de l'eau jusqu'aux épaules, maintenant."
    pr "Je suis obligé de garder les bras tendus pour éviter de noyer la lampe et le téléphone."
    pr "Cette situation commence à m'inquiéter."

    scene b_choix with fx_trans

menu:
    "Insistez !":
        jump dial830
    "Une minute…":
        jump dial820

label dial820:
    scene b_tunnel with fx_trans
    pov "Faites une pause, Michel."
    pov "Il est peut-être plus prudent de réfléchir un peu."
    call attenteTxt(2) from _call_attenteTxt_71

    pr "Vous avez raison."
    pr "Si ça se trouve, avec l'escalier je serais déjà dans la crypte."
    pr "Et au sec, surtout."
    pr "Bon, puis-je..."
    pause (1)
    pr "Un instant..."

    call attenteTxt(3) from _call_attenteTxt_72

    pr "Vous avez entendu ?"
    pr "Non, bien sûr. Suis-je bête."
    pr "Je..."
    
    call attenteTxt(2) from _call_attenteTxt_73

    pr "Je crois qu'il y a quelque chose derrière moi."
    pr "J'entend des bruits étranges."
    pause(2)
    pr "Un clapotis."
    pr "Et comme un souffle."
    pr "Je ne suis pas seul, ici !"
    pause(1)
    pr "Ça se rapproche."
    pr "Ça vient vers moi !"
    pause(2)
    pr "Oh mon Dieu !"
    pr "Je..."

    scene b_choix with fx_trans

menu:
    "Ne paniquez pas.":
        jump dial823
    "Filez de là !":
        $choixDial = True
        jump dial824

label dial823:
    scene b_tunnel with fx_trans
    pov "Ne paniquez pas, Michel. Gardez votre calme."
    call attenteTxt(2) from _call_attenteTxt_74

    pr "Facile à dire !"
    pr "Oh, non !"
    pr "Rien à faire, je file d'ici !"

    jump dial824

label dial824:
    scene b_tunnel with fx_trans
    if $choixDial == True:
        pov "Filez de là ! N’insistez pas !"
    call attenteTxt(2) from _call_attenteTxt_75

    pr "Allez !"
    pr "Ah, ma manche s'accroche !"
    pr "Je... Je glisse !"
    pause(1)
    pr "Je ne peux pas m'agripper !"
    pause(1)
    pr "Non !"
    pr "Je... Glubs..."
    pr "Respirer... Gloup..."
    pr "Resp... Noy..."
    pr "..."

    jump dialFIN04

label dial830:
    scene b_tunnel with fx_trans
    pov "Tant que vous le pouvez, avancez."
    pov "Ça ne peut pas continuer comme ça encore longtemps."
    call attenteTxt(2) from _call_attenteTxt_76

    pr "D'accord."
    pr "Je vais essayer."
    pr "Mais ça devient vraiment compliqué, avec le téléphone et la torche."
    pr "Ne vous formalisez pas, mais je préfère voir où je vais que de papoter avec vous."
    pr "Je vous recontacte dès que possible."

    call attenteTxt(5) from _call_attenteTxt_77

    pr "Ah, bon sang !"
    pause(2)
    pr "J'ai vraiment cru ne pas y arriver."
    pr "Et que ce satané tunnel n'ait pas de fin."
    pr "Mais, me voilà."
    scene b_crypte with fx_trans
    pr "Je suis dans la crypte."
    pause(2)
    pr "C'est immense."
    pr "Et froid."
    pr "En plus de ce fichu livre, je risque de ramener également une pneumonie."
    call attenteTxt(2) from _call_attenteTxt_78
    pr "En parlant du livre."
    $ renpy.say(pr, "Oh, " + str(joueur) + ", il est là !")
    pr "Il repose au centre du mausolée, sur un piédestal."
    pr "Il est magnifique. La couverture est sertie de plaques dorées et ouvragées."
    pr "Lorsque je l'éclaire avec ma torche, cela jette des éclats merveilleux."
    pause(2)
    pr "Je m'approche."

    sys "--->dial900 à rédiger"
    return

label dial900:
    scene b_crypte with fx_trans
    call attenteTxt(2) from _call_attenteTxt_79

    pr "Il est encore plus beau de près."
    pr "Sa couverture semble faite de bois sombre, serti de plaques d'or."
    pr "Au centre, on a gravé le titre."
    pr "«{i}Libris Daemonaticaspius{/i}»"
    pr "C'est bien lui."
    pr "Mon cœur bat la chamade."
    pr "J'espère que je ne vais par faire un malaise."

    scene b_choix with fx_trans

menu:
    'Ouvrez-le.':
        jump dial901
    'Prenez-le.':
        jump dial903

label dial901:
    scene b_crypte with fx_trans
    pov "Allez-y, ouvrez-le et cherchez le passage qui parle du portail de sortie."
    call attenteTxt(2) from _call_attenteTxt_80

    pr "Hmmm. Il y a un fermoir."
    pr "Ah. Ça n'est qu'un simple loquet."
    pr "Oh, quel magnifique vélin. Ou bien, est-ce du parchemin ?"
    pr "Je n'arrive pas bien à voir, dans cette pénombre."
    pr "Peu importe… Voyons..."

    call attenteTxt(3) from _call_attenteTxt_81

    pr "Ah, voilà."
    pr "L’illustration de ce passage représente le même disque bleu que sur la frise."
    pr "Je pense qu'il s'agit du moyen qu'employait Jean de Malemont pour s'échapper."

    jump dial904

label dial903:
    scene b_crypte with fx_trans
    pov "Emparez-vous en. Vous aurez le temps de le lire plus tard."
    call attenteTxt(2) from _call_attenteTxt_82

    pr "Oui, vous avez raison."
    pr "Mieux vaut le sortir d'ici, hors de toute cette humidité."
    pr "Et trouver un coin tranquille pour bouquiner."
    pause(1)
    pr "Han !"
    pr "Bouh, qu'il est lourd !"

    jump dial904

label dial904:
    scene b_crypte with fx_trans
    call attenteTxt(2) from _call_attenteTxt_83

    pr "Voyons, je..."
    pause(3)
    pr "Qu'est-ce que..."
    pr "Oh, non !"

    scene b_choix with fx_trans

menu:
    'Le livre est abimé ?':
        $choixDial = False
        jump dial905
    'Que se passe-t-il ?':
        $choixDial = True
        jump dial905

label dial905:
    scene b_crypte with fx_trans
    if choixDial == False:
        pov "Qu'y a-t-il ? Vous l'avez endommagé ?"
    else:
        pov "Michel ? Que se passe-t-il ? Parlez-moi !"
    call attenteTxt(2) from _call_attenteTxt_84

    pr "Non, ce n'est pas ça, je..."
    pause(1)
    pr "L'Ombre. Celle des peintures."
    pr "Elle est ici."
    pr "Devant moi." #//TODO ajouter variable pour 'à nouveau devant moi'
    pr "On dirait un tourbillon de fumée, s'enroulant sur lui même à l'infini."
    pr "C'est hypnotique."
    pause(1)
    pr "Et atroce à regarder trop longtemps."
    pr "Je ne sais pas quoi faire."
    pr "Elle est là, immobile, à quelques mètres de moi."
    pause(2)
    pr "On dirait qu'elle attend quelque chose."
    pr "Le livre peut-être ?"

    scene b_choix with fx_trans

menu:
    'Lisez le livre !':
        jump dial910
    'Parlez-lui.':
        jump dial950
    'Fonçez !':
        jump dial980

label dial910:
    scene b_crypte with fx_trans
    pov "Ne perdez pas une minute, Michel."
    pov "Lisez le passage sur le moyen de vous échapper."
    call attenteTxt(2) from _call_attenteTxt_85

    pr "Oui, vous avez raison."
    pr "Je cherche le bon chapitre."

    call attenteTxt(4) from _call_attenteTxt_86

    pr "Le voilà."
    pr "Oh, bon sang."
    pause(2)
    pr "L'Ombre ne bouge pas, mais elle me flanque une de ces frousses !"

    scene b_choix with fx_trans

menu:
    'Ressaisissez-vous !':
        jump dial912
    'Ne la regardez pas !':
        jump dial913

label dial912:
    scene b_crypte with fx_trans
    pov "Ressaisissez-vous, Michel ! Ne vous laissez pas impressionner."
    call attenteTxt(2) from _call_attenteTxt_87

    pr "Oui, oui !"
    pr "Ah, voilà..."

    jump dial914

label dial913:
    scene b_crypte with fx_trans
    pov "Ne regardez pas l’Ombre, Michel. Concentrez-vous !"
    call attenteTxt(2) from _call_attenteTxt_88

    pr "Vous avez raison."
    pr "J'ai l'impression que ça pourrait me rendre fou."
    
    jump dial914

label dial914:
    scene b_crypte with fx_trans
    call attenteTxt(2) from _call_attenteTxt_89

    pr "J’ai trouvé le bon passage. Je le lis…"
    pr "Hum…"

    call attenteTxt(2) from _call_attenteTxt_90

    pr "{i}Mau...nai, ihim...{/i}"
    pr "Hum."
    pause(2)
    pr "Étrange, ça ne semble pas être du latin."
    pr "Bon..."
    pause(3)
    pr "«{i}Maunai ihim tireurepa te, Artlu ihim tipicca non !{/i}»"

    call attenteTxt(5) from _call_attenteTxt_91

    pr "L'Ombre s'est figée."
    pr "Je veux dire, même le mouvement de fumée qui l'anime s'est ralenti."
    pr "Oh !"
    pause(3)
    pr "Elle grandit. Elle se met à enfler."
    pr "Et elle s'arrondit."
    pr "La fumée semble se solidifier à mesure qu'elle prend la forme d'un disque."
    pr "Et... Whow !"

    call attenteTxt(3) from _call_attenteTxt_92

    pr "Ça s'est ouvert. En son centre."
    pr "L'Ombre s'est transformée en grand disque noir."
    pr "Et maintenant, son centre s'est creusé, formant une sorte de siphon."
    pr "Il se met à briller."
    pause(2)
    pr "C'est magnifique."
    pr "La lueur est d'un bleu doux, comme un ciel d'été."
    pr "C'est la sortie ! Je vais pouvoir sortir !"

    scene b_choix with fx_trans

menu:
    'Allez-y !':
        jump dial917
    'Attendez !':
        jump dial918

label dial917:
    scene b_crypte with fx_trans
    pov "Allez-y, Michel ! Et quand vous serez libre, appelez-moi !"
    call attenteTxt(2) from _call_attenteTxt_93

    pr "Oui !"
    pr "Merci, mon ami !"
    pr "Je vous retrouve de l'autre côté."
    $ renpy.say(pr, "À très bientôt, " + str(joueur) + " !")

    jump dial919

label dial918:
    scene b_crypte with fx_trans
    pov "Soyez prudent ! Qui vous dit que c'est bien ce que vous pensez ?"
    call attenteTxt(2) from _call_attenteTxt_94

    pr "Ne vous inquiétez pas."
    pr "Je suis confiant."
    pr "Je suis sûr que c'est le passage."
    pr "Jean a dû l'utiliser un bon nombre de fois."

label dial919:
    scene b_crypte with fx_trans
    call attenteTxt(2) from _call_attenteTxt_95

    pr "Je vais garder le téléphone à la main, peut-être ne seront-nous pas coupés."
    pr "J'y vais."

    call attenteTxt(3) from _call_attenteTxt_96

    pr "Plus je m'approche, et plus je me sens attiré."
    pr "Comme si le disque exerçait sa propre gravité."
    pr "C'est hypnotique, mais pas effrayant. Pas comme l'Ombre, avant sa transformation."
 
    call attenteTxt(3) from _call_attenteTxt_97

    pr "Je crois que je ne vais plus pouvoir résister longtemps."
    pr "Je vais être aspiré."
    pr "Merci, merci pour votre aide !"
    pr "Je commence à voir l'autre côté."
    pr "C'est..."
    pause(3)
    pr "L'attraction est trop forte, maintenant."
    pr "Mais..."
    pause(2)
    pr "Ça ne ressemble pas..."
    pause(1)
    pr "Ce n'est pas notre monde !"
    pr "Noooon !"

    jump dialFIN05

label dial950:
    scene b_crypte with fx_trans
    pov "Peut-être n'est-elle pas hostile ?"
    pov "Essayez de communiquer avec elle, on ne sait jamais."
    call attenteTxt(2) from _call_attenteTxt_98

    pr "Vous avez peut-être raison."
    pr "Je vais lui parler."
    pr "Ça a beau être un fantôme, ou un ectoplasme, ou encore le spectre de Malemont..."

    scene b_choix with fx_trans

menu:
    'Parlez-lui.':
        $choixDial = False
        jump dial951
    'Dépéchez-vous !':
        $choixDial = True
        jump dial951

label dial951:
    scene b_crypte with fx_trans
    if choixDial == False:
        pov "Allez-y. Parlez-lui, bon sang !"
    else:
        pov "Ne perdez pas de temps. Parlez-lui !"
    call attenteTxt(2) from _call_attenteTxt_99

    pr "Oui, pardon."
    pr "Désolé, je m'égare."
    pr "Allons-y."
    pause(1)
    pr "Je prend une bonne respiration."

    call attenteTxt(2) from _call_attenteTxt_100

    pr "— Salutations, ô Entité !"

    scene b_choix with fx_trans

menu:
    'Quelle intro !':
        $choixDial = False
        jump dial953
    '« Entité » ?':
        $choixDial = True
        jump dial953

label dial953:
    scene b_crypte with fx_trans
    if choixDial == False:
        pov "Quelle intro !"
        pov "Bon sang, Michel. Quel orateur vous faites !"
    else:
        pov "« {i}Entité{/i} » ? Sérieusement ?"
    call attenteTxt(2) from _call_attenteTxt_101

    pr "Chut, taisez-vous !"
    pr "Ne la vexez pas."
    
    call attenteTxt(2) from _call_attenteTxt_102

    pr "— {i}Je ne désire pas vous déranger.{/i}"
    pr "— {i}Peut-être protégez-vous le trésor de Malemont.{/i}"
    pr "— {i}Je ne cherche qu'à sortir d'ici.{/i}"

    call attenteTxt(2) from _call_attenteTxt_103

    pr "Je crois qu'elle m'a compris."
    pr "Elle..."
    pause(2)
    pr "Elle bouge."

    call attenteTxt(1) from _call_attenteTxt_104
    
    pr "Elle..."
    pause(1)
    pr "Elle fonce sur moi !"
    pr "Je ne sais pas où..."
    pause(2)
    pr "Oh, non !"
    pause(1)
    pr "Elle m'enveloppe !"
    pr "Quelle horreur !"
    pr "Ahhhhh... !"

    call attenteTxt(3) from _call_attenteTxt_105
    
    pr "Ahhhh !"
    pr "Ahhh ha ha ha !"
    pr "Héhéhéhéhé..."
    pause(3)
    pr "Il est bleu !"
    pr "Tout est bleu !"
    pause(4)
    pr "Un bleu horrible, sanguin !"
    pr "Ha ha ha ha !"

    jump dialFIN06

label dial980:
    scene b_crypte with fx_trans
    pov "Filez de là, sauvez-vous ! Vite !"
    call attenteTxt(2) from _call_attenteTxt_106

    pr "Oui !"
    pr "Je vais la contourner."
    pause(3)
    pr "...Courir..."
    pause(2)
    pr "...Pas de sortie..."
    pause(3)
    pr "...Je glisse..."
    pr "Ahhhh !"
    pause(1)
    pr "Ma cheville !"
    pr "...Peut plus marcher..."
    
    call attenteTxt(3) from _call_attenteTxt_107

    pr "Elle arrive !"
    pr "...Peut pas me défendre..."
    pr "...Je..."
    pause(3)
    pr "Oh mon Dieu !"
    pr "Elle m'attrape !"
    pr "Ahhh ! La douleur !"
    pr "Elle me tire ! J'ai mal !"
    pr "Ahhhh !"

    jump dialFIN07